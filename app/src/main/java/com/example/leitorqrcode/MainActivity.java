package com.example.leitorqrcode;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.leitorqrcode.model.Presenca;
import com.example.leitorqrcode.repository.AlunoRepository;
import com.example.leitorqrcode.repository.PresencaRepository;
import com.google.firebase.auth.FirebaseAuth;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private ImageView btnScan;
    private PresencaRepository presencaRepository;
    private AlunoRepository alunoRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnScan = findViewById(R.id.btnScan);
        final Activity activity = this;
        presencaRepository = new PresencaRepository();
        alunoRepository = new AlunoRepository();

        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator integrator = new IntentIntegrator(activity);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
                integrator.setPrompt("Camera Scan");
                integrator.setCameraId(0);
                integrator.initiateScan();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result!=null){
            if(result.getContents()!=null){
                Presenca presenca = new Presenca();
                String idUsuario = FirebaseAuth.getInstance().getCurrentUser().getUid();
                alunoRepository.getCodigoAluno(idUsuario, new AlunoRepository.callbackCodigo() {
                    @Override
                    public void onCallback(String codigoAluno) {
                        if(codigoAluno != null) {
                            presenca.setCodigoAluno(codigoAluno);
                            String[] texto = result.getContents().split("-");
                            presenca.setMateria(texto[0].trim());
                            Date dataAtual = new Date();
                            presenca.setDataEntrada(dataAtual);
                            presencaRepository.add(presenca);
                        } else {
                            alert("Falha ao salvar a presença do aluno.");
                        }
                    }
                });

            } else {
                alert("Erro de leitura");
            }
        }
    }

    private void alert(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }
}