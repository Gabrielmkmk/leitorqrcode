package com.example.leitorqrcode.model;

import java.util.Date;

public class Presenca {
    String codigoAluno;
    String materia;
    Date dataEntrada;
    Date dataSaida;
    Date dataEntrada2;
    Date dataSaida2;
    Date dataEntrada3;
    Date dataSaida3;

    public Presenca(){}

    public Presenca(String codigoAluno, String materia, Date dataEntrada, Date dataSaida, Date dataEntrada2, Date dataSaida2, Date dataEntrada3, Date dataSaida3) {
        this.codigoAluno = codigoAluno;
        this.materia = materia;
        this.dataEntrada = dataEntrada;
        this.dataSaida = dataSaida;
        this.dataEntrada2 = dataEntrada2;
        this.dataSaida2 = dataSaida2;
        this.dataEntrada3 = dataEntrada3;
        this.dataSaida3 = dataSaida3;
    }

    public String getCodigoAluno() {
        return codigoAluno;
    }

    public void setCodigoAluno(String codigoAluno) {
        this.codigoAluno = codigoAluno;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public Date getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(Date dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public Date getDataSaida() {
        return dataSaida;
    }

    public void setDataSaida(Date dataSaida) {
        this.dataSaida = dataSaida;
    }

    public Date getDataEntrada2() {
        return dataEntrada2;
    }

    public void setDataEntrada2(Date dataEntrada) {
        this.dataEntrada2 = dataEntrada;
    }

    public Date getDataSaida2() {
        return dataSaida2;
    }

    public void setDataSaida2(Date dataSaida) {
        this.dataSaida2 = dataSaida;
    }

    public Date getDataEntrada3() {
        return dataEntrada3;
    }

    public void setDataEntrada3(Date dataEntrada) {
        this.dataEntrada3 = dataEntrada;
    }

    public Date getDataSaida3() {
        return dataSaida3;
    }

    public void setDataSaida3(Date dataSaida) {
        this.dataSaida3 = dataSaida;
    }
}
