package com.example.leitorqrcode.model;

public class Aluno {
    String nome;
    String sobrenome;
    String codigo;
    String dataNascimento;
    String telefone;

    public Aluno(){}

    public Aluno(String nome, String sobrenome, String codigo, String dataNascimento, String telefone) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.codigo = codigo;
        this.dataNascimento = dataNascimento;
        this.telefone = telefone;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
