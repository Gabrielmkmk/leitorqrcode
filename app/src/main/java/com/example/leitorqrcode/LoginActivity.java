package com.example.leitorqrcode;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.leitorqrcode.repository.AlunoRepository;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Date;

public class LoginActivity extends AppCompatActivity {

    private EditText editEmail, editSenha;
    private Button btnEntrar, btnCadastrar;
    private AlunoRepository alunoRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editEmail = findViewById(R.id.edtEmail);
        editSenha = findViewById(R.id.edtSENHA);
        btnEntrar = findViewById(R.id.btnENTRAR);
        btnCadastrar = findViewById(R.id.btnCadastrar);
        alunoRepository = new AlunoRepository();

        btnEntrar.setOnClickListener(view -> {
            String emailLogin = editEmail.getText().toString();
            String senhaLogin = editSenha.getText().toString();

            if(emailLogin.isEmpty() || senhaLogin.isEmpty()) {
                Toast.makeText(getApplicationContext(),"Preencha todos os campos!",Toast.LENGTH_SHORT).show();
            } else {
                FirebaseAuth.getInstance().signInWithEmailAndPassword(emailLogin, senhaLogin).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            String idUsuario = FirebaseAuth.getInstance().getCurrentUser().getUid();
                            alunoRepository.getCodigoAluno(idUsuario, new AlunoRepository.callbackCodigo() {
                                @Override
                                public void onCallback(String codigoAluno) {
                                    if(codigoAluno != "") {
                                        trocarDeTela(MainActivity.class);
                                    } else {
                                        Toast.makeText(getApplicationContext(),"Autenticação inválida!",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(getApplicationContext(),"Autenticação inválida!",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        btnCadastrar.setOnClickListener(view -> trocarDeTela(CadastroActivity.class));
    }

    public void trocarDeTela(Class tela){
        Intent trocarTela = new Intent(LoginActivity.this,tela);
        startActivity(trocarTela);
    }
}