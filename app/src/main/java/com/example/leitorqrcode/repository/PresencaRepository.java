package com.example.leitorqrcode.repository;

import androidx.annotation.NonNull;

import com.example.leitorqrcode.model.Presenca;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class PresencaRepository {
    private DatabaseReference databaseReference;
    private FirebaseDatabase db;

    public PresencaRepository() {
        db = FirebaseDatabase.getInstance();
        databaseReference = db.getReference(Presenca.class.getSimpleName());
    }

    public void add(Presenca presenca) {
        presencaExiste(presenca);
    }

    public void presencaExiste(Presenca presenca) {
        final int[] flag = {0};
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        databaseReference.child(presenca.getMateria() + "-" + simpleDateFormat.format(new Date())).child(presenca.getCodigoAluno()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Presenca presencaBanco = snapshot.getValue(Presenca.class);
                if(presencaBanco == null) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    databaseReference.child(presenca.getMateria() + "-" + simpleDateFormat.format(new Date())).child(presenca.getCodigoAluno()).setValue(presenca);
                    flag[0] = 1;
                } else if(flag[0] == 0){
                    long quantidade = snapshot.getChildrenCount();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date dataAtual = new Date();

                    if(quantidade == 3) {
                        presenca.setDataSaida(dataAtual);
                        HashMap p = new HashMap();
                        p.put("dataSaida", dataAtual);
                        databaseReference.child(presenca.getMateria() + "-" + simpleDateFormat.format(new Date())).child(presenca.getCodigoAluno()).updateChildren(p);
                        flag[0] = 1;
                    } else if(quantidade == 4) {
                        presenca.setDataEntrada2(dataAtual);
                        HashMap p = new HashMap();
                        p.put("dataEntrada2", dataAtual);
                        databaseReference.child(presenca.getMateria() + "-" + simpleDateFormat.format(new Date())).child(presenca.getCodigoAluno()).updateChildren(p);
                        flag[0] = 1;
                    } else if(quantidade == 5) {
                        presenca.setDataSaida2(dataAtual);
                        HashMap p = new HashMap();
                        p.put("dataSaida2", dataAtual);
                        databaseReference.child(presenca.getMateria() + "-" + simpleDateFormat.format(new Date())).child(presenca.getCodigoAluno()).updateChildren(p);
                        flag[0] = 1;
                    } else if(quantidade == 6) {
                        presenca.setDataEntrada3(dataAtual);
                        HashMap p = new HashMap();
                        p.put("dataEntrada3", dataAtual);
                        databaseReference.child(presenca.getMateria() + "-" + simpleDateFormat.format(new Date())).child(presenca.getCodigoAluno()).updateChildren(p);
                        flag[0] = 1;
                    } else if(quantidade >= 7) {
                        presenca.setDataSaida3(dataAtual);
                        HashMap p = new HashMap();
                        p.put("dataSaida3", dataAtual);
                        databaseReference.child(presenca.getMateria() + "-" + simpleDateFormat.format(new Date())).child(presenca.getCodigoAluno()).updateChildren(p);
                        flag[0] = 1;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
