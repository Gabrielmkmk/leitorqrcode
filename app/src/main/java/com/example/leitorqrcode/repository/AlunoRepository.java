package com.example.leitorqrcode.repository;

import androidx.annotation.NonNull;

import com.example.leitorqrcode.model.Aluno;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AlunoRepository {
    private DatabaseReference databaseReference;
    private FirebaseDatabase db;

    public AlunoRepository() {
        db = FirebaseDatabase.getInstance();
        databaseReference = db.getReference(Aluno.class.getSimpleName());
    }

    public interface callbackCodigo {
        void onCallback(String codigoAluno);
    }

    public void getCodigoAluno(String idUsuario, callbackCodigo myCallback) {
        databaseReference.child(idUsuario).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Aluno aluno = snapshot.getValue(Aluno.class);
                if(aluno != null) {
                    myCallback.onCallback(aluno.getCodigo());
                } else {
                    myCallback.onCallback("");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public Task<Void> add(Aluno aluno) {
        String idUsuario = FirebaseAuth.getInstance().getCurrentUser().getUid();
        return databaseReference.child(idUsuario).setValue(aluno);
    }
}
